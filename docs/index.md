# The LKCamp documentation

This is the LKCamp documentation written by Helen Koike and Gabriel
Krisman Bertazi.

Copyright (c) 2018 Helen Fornazier <helen.fornazier@gmail.com>

Copyright (c) 2018 Gabriel Krisman Bertazi <gabriel@krisman.be>

This document is licensed, including subpages under the Creative Commons
Attribution-ShareAlike 4.0 International license.  You should have
received a copy of the license along with this work.  If not, see
<http://creativecommons.org/licenses/by-sa/4.0/>.

